package project.university.catolica.models;

import project.university.catolica.enums.TipoFuncionario;
import project.university.catolica.interfaces.ImpostoDeRenda;


public class Diretor extends Funcionario implements ImpostoDeRenda {

	private Float tempoDeCasa;

	private Diretor(String nome, String matricula, String cpf, Float salario, Float tempoDeCasa) {
		super(nome, matricula, cpf, salario);
		this.tempoDeCasa = tempoDeCasa;
		super.tipoFuncionario = TipoFuncionario.DIRETOR;
	}

	public static Diretor criarDiretor(String nome, String matricula, String cpf, Float salario, Float tempoDeCasa) {
		return new Diretor(nome, matricula, cpf, salario, tempoDeCasa);
	}

	public Float getTempoDeCasa() {
		return tempoDeCasa;
	}

	public void setTempoDeCasa(Float tempoDeCasa) {
		this.tempoDeCasa = tempoDeCasa;
	}

	@Override
	public Float getImpostoDeRenda() {
		return getSalario() * 0.23f;
	}


	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
