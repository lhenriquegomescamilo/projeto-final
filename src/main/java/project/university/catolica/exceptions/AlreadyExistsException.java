package project.university.catolica.exceptions;

public class AlreadyExistsException extends Exception {


	public AlreadyExistsException(final String message) {
		super(message);
	}

}
