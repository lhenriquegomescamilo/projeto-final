package project.university.catolica.utils;

import project.university.catolica.enums.TipoFuncionario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReaderUtil {

	private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	private TipoFuncionario tipoFuncionarios;

	public ReaderUtil(TipoFuncionario professor) {
		this.tipoFuncionarios = professor;
	}

	public String lerNomeDeString() throws IOException {
		LoggerUtil.println(String.format("Informe o nome do %s", tipoFuncionarios.getTipo()));
		return reader.readLine();
	}

	public String lerMatriculaDeString() throws IOException {
		LoggerUtil.println(String.format("Informe o matricula do %s", tipoFuncionarios.getTipo()));
		return reader.readLine();
	}

	public String lerCpfDeString() throws IOException {
		LoggerUtil.println(String.format("Informe o CPF do %s", tipoFuncionarios.getTipo()));
		String cpf = reader.readLine();
		if (!CpfUtil.isCPF(cpf)) throw new IllegalArgumentException("CPF inválido");
		return cpf;
	}

	public Float lerSalarioDeString() {
		LoggerUtil.println(String.format("Informe o Salário do %s no formato R$ 10000.00", tipoFuncionarios.getTipo()));
		return readerFromFloat("Salário inválido");

	}

	private Float readerFromFloat(final String mensagem) {
		try {
			return Float.parseFloat(reader.readLine());
		} catch (Exception e) {
			throw new IllegalArgumentException(mensagem);
		}
	}

	public static BufferedReader getReader() {
		return reader;
	}

	public Float lerTempoCasa() {
		LoggerUtil.println(String.format("Informe o tempo de casa do %s em anos, exemplo 1 ano ou 0.1 para meses", tipoFuncionarios.getTipo()));
		return readerFromFloat("O campo tempo de casa está inválido, formato aceito é 1(ano) ou 0.1 para meses");
	}
}
