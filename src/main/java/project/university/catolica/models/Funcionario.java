package project.university.catolica.models;

import project.university.catolica.enums.TipoFuncionario;
import project.university.catolica.utils.CpfUtil;

import java.util.Objects;

public class Funcionario {

	private String nome;
	private String matricula;
	private String cpf;
	private Float salario;
	TipoFuncionario tipoFuncionario;

	Funcionario(String nome, String matricula, String cpf, Float salario) {
		this.nome = nome;
		this.matricula = matricula;
		this.cpf = cpf;
		this.salario = salario;
	}


	public void setSalario(Float salario) {
		this.salario = salario;
	}

	public String getCpfFormatado() {
		return CpfUtil.imprimeCPF(cpf);
	}

	public String getNome() {
		return nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public Float getSalario() {
		return salario;
	}

	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (!(o instanceof Funcionario)) return false;
		final Funcionario funcionario = (Funcionario) o;
		return Objects.equals(getMatricula(), funcionario.getMatricula());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getMatricula());
	}
}
