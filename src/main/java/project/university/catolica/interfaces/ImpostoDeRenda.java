package project.university.catolica.interfaces;

public interface ImpostoDeRenda {

	Float getImpostoDeRenda();
}
