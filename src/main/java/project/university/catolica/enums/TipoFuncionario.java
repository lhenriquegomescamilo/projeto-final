package project.university.catolica.enums;

public enum TipoFuncionario {
	PROFESSOR("Professor(a)", 'P'),
	DIRETOR("Diretor(a)", 'D');

	private final String tipo;
	private final Character chave;

	TipoFuncionario(String tipo, Character chave) {
		this.tipo = tipo;
		this.chave = chave;
	}

	public String getTipo() {
		return tipo;
	}

	public Character getChave() {
		return chave;
	}
}
