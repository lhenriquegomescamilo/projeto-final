package project.university.catolica.utils;

import project.university.catolica.models.Funcionario;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class TableUtil<T extends Funcionario> {

	private List<T> funcionarios;
	private Function<T, String[]> function;
	private String[] headers;

	private TableUtil() {

	}

	public static <R extends Funcionario> TableUtil<R> createInstance() {
		return new TableUtil<>();
	}

	public TableUtil<T> setFuncionarios(List<T> funcionarios) {
		this.funcionarios = funcionarios;
		return this;
	}

	public TableUtil<T> setFunction(Function<T, String[]> function) {
		this.function = function;
		return this;
	}

	public TableUtil<T> setHeaders(String[] headers) {
		this.headers = headers;
		return this;
	}

	public void criarTabela() {
		Objects.requireNonNull(funcionarios, "Funcionarios é obrigatório");
		Objects.requireNonNull(function, "function é obrigatório");
		Objects.requireNonNull(headers, "headers é obrigatório");

		final CommandLineTableUtil tableUtil = new CommandLineTableUtil();
		tableUtil.setShowVerticalLines(true);
		tableUtil.setHeaders(headers);
		for (final T funcionario : funcionarios) {
			final String[] cells = function.apply(funcionario);
			tableUtil.addRow(cells);
		}
		tableUtil.print();

	}

}
