package project.university.catolica.interfaces;

import project.university.catolica.models.Funcionario;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface Controller<T extends Funcionario> {

	T cadastrar() throws Exception;

	List<T> listar();

	T remover() throws IOException;
}
