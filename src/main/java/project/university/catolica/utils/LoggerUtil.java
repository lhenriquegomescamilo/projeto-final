package project.university.catolica.utils;

import java.io.PrintStream;

public class LoggerUtil {

	private static final PrintStream output = System.out;
	private static final PrintStream outputError = System.err;


	public static void println(final String message) {
		output.println(message);
	}

	public static void printError(final String message) {
		outputError.println(message);
	}
}
