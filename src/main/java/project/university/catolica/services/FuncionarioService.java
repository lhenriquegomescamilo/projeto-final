package project.university.catolica.services;

import project.university.catolica.enums.TipoFuncionario;
import project.university.catolica.exceptions.AlreadyExistsException;
import project.university.catolica.models.Diretor;
import project.university.catolica.models.Funcionario;
import project.university.catolica.models.Professor;
import project.university.catolica.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FuncionarioService {

	private static final List<Funcionario> funcionarios = new ArrayList<>();

	public Professor cadastrarProfessor(final Professor professor) throws AlreadyExistsException {
		Objects.requireNonNull(professor, "O campo professor não pode ser vazio");
		cadastro(professor);
		return professor;

	}

	private void lancarExcecaoQuandoMatriculaJaExiste(final Funcionario funcionario) throws AlreadyExistsException {
		if (funcionarios.contains(funcionario)) throw new AlreadyExistsException(String.format(
				"Funcionario do tipo %s com a mtricula %s já existe",
				funcionario.getTipoFuncionario().getTipo(),
				funcionario.getMatricula()
		));
	}

	public List<Professor> buscarTodosProfessores() {
		final Predicate<Funcionario> porProfessores = p -> p.getTipoFuncionario().equals(TipoFuncionario.PROFESSOR);
		return funcionarios
				.stream()
				.filter(porProfessores)
				.map(Professor.class::cast)
				.collect(Collectors.toList());
	}

	public Professor buscarProfessoresPorMatricula(final String matricula) {
		Objects.requireNonNull(matricula, "O campo matrícula não pode ser vazio");
		return funcionarios.stream()
				.filter(professor -> professor.getTipoFuncionario().equals(TipoFuncionario.PROFESSOR))
				.filter(professor -> professor.getMatricula().equalsIgnoreCase(matricula))
				.map(Professor.class::cast)
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException(String.format("Matrícula %s do professor não encontrado", matricula)));
	}

	public void removerPorMatricula(Funcionario funcionario) {
		funcionarios.remove(funcionario);
	}

	public Diretor cadastrarDiretor(final Diretor diretor) throws AlreadyExistsException {
		Objects.requireNonNull(diretor, "O campo diretor não pode ser vazio");
		cadastro(diretor);
		return diretor;
	}

	private void cadastro(final Funcionario funcionario) throws AlreadyExistsException {
		lancarExcecaoQuandoMatriculaJaExiste(funcionario);
		funcionarios.add(funcionario);
		LoggerUtil.println(String.format("======== %s %s cadastrado com sucesso ========== ", funcionario.getTipoFuncionario().getTipo(), funcionario.getNome()));
		LoggerUtil.println("=============================================================\n\n");
	}

	public List<Diretor> buscarTodosDiretores() {
		return funcionarios.stream()
				.filter(funcionario -> funcionario.getTipoFuncionario().equals(TipoFuncionario.DIRETOR))
				.map(Diretor.class::cast)
				.collect(Collectors.toList());
	}

	public Diretor buscarDiretorPorMatricula(final String matricula) {
		return funcionarios.stream()
				.filter(funcionario -> funcionario.getMatricula().equalsIgnoreCase(matricula))
				.map(Diretor.class::cast)
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException(String.format("Matrícula %s do diretor não encontrado", matricula)));
	}
}
