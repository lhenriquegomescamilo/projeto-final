#### Desafio
Em uma universidade tem dois tipos de Funcionários: Diretores ou Professores. Um professor tem nome, matrícula, cpf, salário, ano de admissão. Em professor deve ter um método getImpostoDeRenda, que deverá retornar o imposto de renda do professor (20% do seu salário). Um diretor tem nome, matrícula, cpf, salário e tempo de casa. Em Diretor também tem o método para obter o imposto de renda. (23% do seu salário).

Pegue tudo que for comum às entidades e coloque em uma classe Funcionário, inclusive o método getImpostoDeRenda, para que ele possa ser sobrescrito pelas entidades filhas.

Não é aceito funcionário repetido nesta universidade. Use sobrescrita do equals para auxiliar nesta empreitada e para apresentação use o toString.

Para criar um funcionário, deve ser passada a matrícula no construtor obrigatoriamente.

Trabalhe com um ÚNICO ArrayList de Funcionário.

Receba os dados de vários funcionários e apresente a média do imposto de renda e o total do imposto de renda.

Crie uma entrada de dados validada e protegida contra exceções para todos os tipos primitivos apresentados na Aula 2. Crie uma classe Leitor que ofereça esse serviço para outros programadores utilizarem.


#### Passos para reprodução
1) Primeiro verifique se o projeto possui permissão de execução na sua máquina local
2) Abra uma terminal dentro do projeto
3) Agora que está tudo com o ambiente, agora é necessário compilar o projeto com o comando: `./gradlew build`
4) Com o projeto compilado verifique se a JVM está nas suas variáveis de ambiente, use o comando: `java -version`
5) Para iniciar a aplicação execute o comando: `java -jar ./build/libs/project-university-catolica-0.1.jar` 

##### Observações
* Nesse projeto já possui uma versão pré-definida, se o arquivo **project-university-catolica-0.1.jar** estiver dentro da pasta `./build/libs`
  Execute o **Passo 5** em **passos para reprodução**
