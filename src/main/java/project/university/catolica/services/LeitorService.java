package project.university.catolica.services;

import lombok.Builder;
import project.university.catolica.enums.TipoFuncionario;
import project.university.catolica.models.Diretor;
import project.university.catolica.models.Professor;
import project.university.catolica.utils.ReaderUtil;

import java.io.IOException;
import java.io.Reader;

public class LeitorService {


	public Professor lerProfessor() throws IOException {
		final DefaultParams params = lerPropriedadesBases(TipoFuncionario.PROFESSOR);
		return Professor.criarProfessor(params.nome, params.matricula, params.cpf, params.salario);
	}

	private DefaultParams lerPropriedadesBases(TipoFuncionario tipoFuncionario) throws IOException {
		final ReaderUtil reader = new ReaderUtil(tipoFuncionario);
		return DefaultParams.builder()
				.nome(reader.lerNomeDeString())
				.matricula(reader.lerMatriculaDeString())
				.cpf(reader.lerCpfDeString())
				.salario(reader.lerSalarioDeString())
				.build();
	}

	public String lerMatricula() throws IOException {
		return new ReaderUtil(TipoFuncionario.PROFESSOR).lerMatriculaDeString();
	}

	public Diretor lerDiretor() throws IOException {
		TipoFuncionario diretor = TipoFuncionario.DIRETOR;
		final DefaultParams params = lerPropriedadesBases(diretor);
		final Float tempoCasa = new ReaderUtil(diretor).lerTempoCasa();
		return Diretor.criarDiretor(params.nome, params.matricula, params.cpf, params.salario, tempoCasa);
	}

	@Builder
	private static class DefaultParams {
		private String nome;
		private String matricula;
		private String cpf;
		private Float salario;
	}

}
