package project.university.catolica.enums;

import project.university.catolica.controllers.DiretorController;
import project.university.catolica.controllers.ProfessorController;
import project.university.catolica.exceptions.AlreadyExistsException;

import java.io.IOException;
import java.util.stream.Stream;

public enum AcoesSistema {
	CADASTRO_PROFESSOR("P1") {
		@Override
		public void aplicarAcao() throws IOException, AlreadyExistsException {
			ProfessorController.getInstance().cadastrar();
		}
	},
	LISTAR_PROFESSOR("P2") {
		@Override
		public void aplicarAcao() {
			ProfessorController.getInstance().listar();
		}
	},
	REMOVER_PROFESSOR("P3") {
		@Override
		public void aplicarAcao() throws Exception {
			ProfessorController.getInstance().remover();
		}
	},

	CADASTRO_DIRETOR("D1") {
		@Override
		public void aplicarAcao() throws Exception {
			DiretorController.getInstance().cadastrar();
		}
	},
	LISTAR_DIRETOR("D2") {
		@Override
		public void aplicarAcao() {
			DiretorController.getInstance().listar();
		}
	},
	REMOVER_DIRETOR("D3") {
		@Override
		public void aplicarAcao() throws IOException {
			DiretorController.getInstance().remover();
		}
	},
	SAIR("SAIR") {
		@Override
		public void aplicarAcao() {
			System.exit(0);
		}
	};

	private String tipoAcao;

	AcoesSistema(final String tipoAcao) {
		this.tipoAcao = tipoAcao;
	}

	public abstract void aplicarAcao() throws Exception;

	public static AcoesSistema recuperarAcao(final String tipoAcao) {
		return Stream.of(AcoesSistema.values())
				.filter(acao -> acao.tipoAcao.equalsIgnoreCase(tipoAcao))
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Operação não disponível"));
	}
}
