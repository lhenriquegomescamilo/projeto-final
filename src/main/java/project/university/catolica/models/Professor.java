package project.university.catolica.models;

import project.university.catolica.enums.TipoFuncionario;
import project.university.catolica.interfaces.ImpostoDeRenda;

public class Professor extends Funcionario implements ImpostoDeRenda {

	private Integer anoDeAdmissao;

	private Professor(String nome, String matricula, String cpf, Float salario) {
		super(nome, matricula, cpf, salario);
		super.tipoFuncionario = TipoFuncionario.PROFESSOR;
	}

	public static Professor criarProfessor(final String nome, final String matricula, final String cpf, final Float salario) {
		return new Professor(nome, matricula, cpf, salario);
	}

	@Override
	public Float getImpostoDeRenda() {
		return getSalario() * 0.2f;
	}

	public Integer getAnoDeAdmissao() {
		return anoDeAdmissao;
	}

	public void setAnoDeAdmissao(Integer anoDeAdmissao) {
		this.anoDeAdmissao = anoDeAdmissao;
	}

	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
