package project.university.catolica.controllers;

import project.university.catolica.exceptions.AlreadyExistsException;
import project.university.catolica.interfaces.Controller;
import project.university.catolica.models.Professor;
import project.university.catolica.services.FuncionarioService;
import project.university.catolica.services.LeitorService;
import project.university.catolica.utils.LoggerUtil;
import project.university.catolica.utils.TableUtil;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class ProfessorController implements Controller<Professor> {

	private FuncionarioService funcionarioService = new FuncionarioService();
	private LeitorService leitorService = new LeitorService();
	private static ProfessorController instance;

	private ProfessorController() {
	}

	public static ProfessorController getInstance() {
		if (Objects.isNull(instance)) {
			instance = new ProfessorController();
		}
		return instance;
	}

	@Override
	public Professor cadastrar() throws IOException, AlreadyExistsException {
		LoggerUtil.println("============================================================= ");
		LoggerUtil.println("========== Seja bem vindo ao cadastro de Professores(as) ========== ");

		return funcionarioService.cadastrarProfessor(leitorService.lerProfessor());
	}

	@Override
	public List<Professor> listar() {
		LoggerUtil.println("============ Professores(as) ============");
		final List<Professor> professores = funcionarioService.buscarTodosProfessores();

		TableUtil.<Professor>createInstance()
				.setFuncionarios(professores)
				.setHeaders(new String[]{"Tipo", "Nome", "Matricula", "Cpf", "Salario", "Imposto de Renda"})
				.setFunction(this::linhaProfessor)
				.criarTabela();
		return professores;
	}

	private String[] linhaProfessor(final Professor professor) {
		return new String[]{
				professor.getTipoFuncionario().getTipo(),
				professor.getNome(),
				professor.getMatricula(),
				professor.getCpfFormatado(),
				String.format("R$ %s", professor.getSalario()),
				String.format("R$ %s", professor.getImpostoDeRenda())
		};
	}

	@Override
	public Professor remover() throws IOException {
		final String matricula = leitorService.lerMatricula();
		final Professor professor = funcionarioService.buscarProfessoresPorMatricula(matricula);
		funcionarioService.removerPorMatricula(professor);
		LoggerUtil.println(String.format("Professor(es) com a matrícula %s foi removido com sucesso!", professor.getMatricula()));
		return professor;
	}
}
