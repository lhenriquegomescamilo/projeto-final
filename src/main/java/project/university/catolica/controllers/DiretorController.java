package project.university.catolica.controllers;

import project.university.catolica.interfaces.Controller;
import project.university.catolica.models.Diretor;
import project.university.catolica.services.FuncionarioService;
import project.university.catolica.services.LeitorService;
import project.university.catolica.utils.LoggerUtil;
import project.university.catolica.utils.TableUtil;

import java.io.IOException;
import java.util.List;

public class DiretorController implements Controller<Diretor> {
	private static final DiretorController intance;
	private static final FuncionarioService funcionarioService;

	private LeitorService leitorService = new LeitorService();

	static {
		intance = new DiretorController();
		funcionarioService = new FuncionarioService();
	}

	private DiretorController() {

	}

	public static DiretorController getInstance() {
		return intance;
	}

	@Override
	public Diretor cadastrar() throws Exception {
		LoggerUtil.println("============================================================= ");
		LoggerUtil.println("========== Seja bem vindo ao cadastro de Diretores(as) ========== ");
		return funcionarioService.cadastrarDiretor(leitorService.lerDiretor());
	}

	@Override
	public List<Diretor> listar() {
		List<Diretor> diretores = funcionarioService.buscarTodosDiretores();
		TableUtil.<Diretor>createInstance()
				.setHeaders(new String[]{"Tipo", "Nome", "Matricula", "Cpf", "Salario", "Imposto de Renda", "Tempo de casa"})
				.setFuncionarios(diretores)
				.setFunction(this::linhaDiretor)
				.criarTabela();
		return diretores;
	}

	private String[] linhaDiretor(final Diretor diretor) {
		return new String[]{
				diretor.getTipoFuncionario().getTipo(),
				diretor.getNome(),
				diretor.getMatricula(),
				diretor.getCpfFormatado(),
				String.format("R$ %s", diretor.getSalario()),
				String.format("R$ %s", diretor.getImpostoDeRenda()),
				String.format("%s ano(s)", diretor.getTempoDeCasa()),

		};
	}

	@Override
	public Diretor remover() throws IOException {
		final String matricula = leitorService.lerMatricula();
		final Diretor diretor = funcionarioService.buscarDiretorPorMatricula(matricula);
		funcionarioService.removerPorMatricula(diretor);
		LoggerUtil.println(String.format("Diretor com matrícula %s foi removido com sucesso!", diretor.getMatricula()));
		return diretor;
	}
}
