package project.university.catolica;

import project.university.catolica.enums.AcoesSistema;
import project.university.catolica.enums.TipoFuncionario;
import project.university.catolica.utils.LoggerUtil;
import project.university.catolica.utils.ReaderUtil;

import java.io.BufferedReader;
import java.util.stream.Stream;

/**
 * Orientações:
 * <p>
 * 1)      Faça o upload do arquivo.java
 * <p>
 * Em uma universidade tem dois tipos de Funcionários: Diretores ou Professores. Um professor tem nome,
 * matrícula, cpf, salário, ano de admissão. Em professor deve ter um método getImpostoDeRenda,
 * que deverá retornar o imposto de renda do professor (20% do seu salário). Um diretor tem nome, matrícula, cpf, salário e tempo de casa.
 * <p>
 * Em Diretor também tem o método para obter o imposto de renda. (23% do seu salário).
 * s
 * Pegue tudo que for comum às entidades e coloque em uma classe Funcionário, inclusive o método getImpostoDeRenda, para que ele possa ser sobrescrito pelas entidades filhas.
 * <p>
 * Não é aceito funcionário repetido nesta universidade. Use sobrescrita do equals para auxiliar nesta empreitada e para apresentação use o toString.
 * <p>
 * Para criar um funcionário, deve ser passada a matrícula no construtor obrigatoriamente.
 * <p>
 * Trabalhe com um ÚNICO ArrayList de Funcionário.
 * <p>
 * Receba os dados de vários funcionários e apresente a média do imposto de renda e o total do imposto de renda.
 * <p>
 * Crie uma entrada de dados validada e protegida contra exceções para todos os tipos primitivos apresentados na Aula 2.
 * Crie uma classe Leitor que ofereça esse serviço para outros programadores utilizarem.
 */
public class Application {
	private static final String SEPARATOR = "============================================================";

	private static BufferedReader reader = ReaderUtil.getReader();


	public static void main(final String[] args) {
		LoggerUtil.println(SEPARATOR);
		LoggerUtil.println("======== Seja bem vindo gerenciador de funcionarios(as) ========");
		LoggerUtil.println(SEPARATOR);
		while (true) menuPrincipal();

	}

	private static void menuPrincipal() {
		String acao = "";
		try {
			LoggerUtil.println("Ações disponíveis");
			LoggerUtil.println("Selecione uma das ações");
			Stream
					.of(TipoFuncionario.PROFESSOR, TipoFuncionario.DIRETOR)
					.forEach(Application::cabecalho);
			LoggerUtil.println("Digite SAIR para sair!");
			acao = reader.readLine();
			AcoesSistema.recuperarAcao(acao).aplicarAcao();
		} catch (final Exception e) {
			LoggerUtil.printError(String.format("Erro ao executar a ação  %s do sistema, reiniciando as ações", acao));
			LoggerUtil.printError(e.getMessage());
		}
	}

	private static void cabecalho(final TipoFuncionario tipoFuncionario) {
		LoggerUtil.println(SEPARATOR);
		LoggerUtil.println(String.format("========== %s ==========", tipoFuncionario.getTipo()));
		LoggerUtil.println(SEPARATOR);
		LoggerUtil.println(String.format("# Digite %s1 para Cadastro de %s", tipoFuncionario.getChave(), tipoFuncionario.getTipo()));
		LoggerUtil.println(String.format("# Digite %s2 para Listar de %s", tipoFuncionario.getChave(), tipoFuncionario.getTipo()));
		LoggerUtil.println(String.format("# Digite %s3 para Remover de %s", tipoFuncionario.getChave(), tipoFuncionario.getTipo()));
	}
}
